package com;

import com.device.Device;
import com.device.Radio;
import com.device.Tv;
import com.remote.AdvancedRemote;
import com.remote.BasicRemote;

public class Demo {
    public static void main(String[] args) {
        testDevice(new Tv());

        testDevice(new Radio());
    }

    private static void testDevice(Device device) {
        System.out.println("Basic remote.");
        BasicRemote basicRemote = new BasicRemote(device);
        basicRemote.power();
        device.printStatus();

        System.out.println("Advanced remote.");
        AdvancedRemote advancedRemote = new AdvancedRemote(device);
        advancedRemote.power();
        advancedRemote.mute();
        device.printStatus();
    }
}
